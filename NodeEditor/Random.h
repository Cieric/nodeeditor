#pragma once

#include <random>

template<typename T>
T Random(T min, T max)
{
    static std::random_device rd;
    static std::mt19937_64 eng(rd());
    return std::uniform_int_distribution<T>{min, max}(eng);
}

template<typename T>
T Random()
{
    auto constexpr min = std::numeric_limits<T>().min();
    auto constexpr max = std::numeric_limits<T>().max();
    return Random<T>(min, max);
}