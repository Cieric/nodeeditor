#pragma once

#include "Node.h"

struct DelayNode : public Node
{
    DelayNode() { throw "never call default constructor"; };

    DelayNode(const char* name, const ImVec2& pos) : Node(name, pos, 0, 1)
    {}

    virtual ImVec2 GetSlotPos(size_t slot_no) const override
    {
        if (slot_no < InputsCount)
            return ImVec2(Pos.x, Pos.y + Size.y * ((float)slot_no + 1) / ((float)InputsCount + 1));
        slot_no -= InputsCount;
        return ImVec2(Pos.x + Size.x, Pos.y + Size.y * ((float)slot_no + 1) / ((float)OutputsCount + 1));
    }

    virtual ImVec2 GetSlotNormal(size_t slot_no) const override
    {
        if (slot_no < InputsCount)
            return ImVec2(-50, 0);
        return ImVec2(50, 0);
    }

    virtual void Render(NodeEditor* editor, NodeKey id, ImVec2 offset) override;

};