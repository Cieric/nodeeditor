#pragma once
#include <imgui.h>
#include "HandleMap.h"
#include "Random.h"

class NodeEditor;

typedef uint32_t NodeKey;

struct NodeLink
{
    NodeKey InputIdx, OutputIdx;
    size_t InputSlot, OutputSlot;
    bool isDelayed = false;

    NodeLink() { throw "never call default constructor"; };

    NodeLink(NodeKey input_idx, size_t input_slot, NodeKey output_idx, size_t output_slot) { InputIdx = input_idx; InputSlot = input_slot; OutputIdx = output_idx; OutputSlot = output_slot; }

    virtual void Render(NodeEditor* editor, ImVec2 offset);
};

static const NodeKey NullKey = -1;

struct Node
{
    char    Name[32];
    ImVec2  Pos, Size;
    //float   Value;
    //ImVec4  Color;
    size_t InputsCount, OutputsCount;
    std::vector<bool> connectedInputs;

    Node() { throw "never call default constructor"; };

    Node(const char* name, const ImVec2& pos, size_t inputs_count, size_t outputs_count)
    {
        strcpy_s(Name, name);
        Pos = pos;
        //Value = value;
        //Color = color;
        InputsCount = inputs_count;
        OutputsCount = outputs_count;
        connectedInputs.resize(inputs_count);
    }

    virtual ImVec2 GetSlotPos(size_t slot_no) const
    {
        if (slot_no < InputsCount)
            return ImVec2(Pos.x, Pos.y + Size.y * ((float)slot_no + 1) / ((float)InputsCount + 1));
        slot_no -= InputsCount;
        return ImVec2(Pos.x + Size.x, Pos.y + Size.y * ((float)slot_no + 1) / ((float)OutputsCount + 1));
    }

    virtual ImVec2 GetSlotNormal(size_t slot_no) const
    {
        if (slot_no < InputsCount)
            return ImVec2(-1, 0);
        return ImVec2(1, 0);
    }

    virtual bool CanConnectNode(size_t slot_no)
    {
        return slot_no >= InputsCount || (slot_no < InputsCount && !connectedInputs[slot_no]);
    }

    virtual bool ConnectNode(size_t slot_no, Node& other, size_t other_slot_no)
    {
        bool isInput = (slot_no < InputsCount);
        bool otherIsInput = (other_slot_no < other.InputsCount);
        if (!(isInput ^ otherIsInput))
            return false;
        if (slot_no < InputsCount)
            connectedInputs[slot_no] = true;
        return true;
    }

    virtual void DisconnectNode(size_t slot_no, Node& other, size_t other_slot_no)
    {
        if (slot_no < InputsCount)
            connectedInputs[slot_no] = false;
    }

    virtual void ContextMenu()
    {

    }

    virtual void Render(NodeEditor* editor, NodeKey id, ImVec2 offset);
};