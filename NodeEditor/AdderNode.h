#pragma once
#include "Node.h"

struct AdderNode : public Node
{
    AdderNode() { throw "never call default constructor"; };

    AdderNode(const char* name, const ImVec2& pos) : Node(name, pos, 2, 1)
    {}

    virtual void Render(NodeEditor* editor, NodeKey id, ImVec2 offset) override;

};