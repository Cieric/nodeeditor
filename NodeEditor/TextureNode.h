#pragma once
#include "Node.h"
#include <imgui.h>

#include "lodepng.h"
#include <GL/glew.h>

#include "ImGuiFileBrowser.h"

struct TextureNode : public Node
{
    ImTextureID imageID;
    unsigned width;
    unsigned height;

    ImGui::FileBrowser filebrowser;

    TextureNode() { throw "never call default constructor"; };

    TextureNode(const char* name, const ImVec2& pos) : Node(name, pos, 1, 1)
    {
        filebrowser = ImGui::FileBrowser();
        LoadImage("Images/test.png");
    }

    void LoadImage(std::string filename)
    {
        std::vector<unsigned char> image;
        unsigned error = lodepng::decode(image, width, height, filename, LCT_RGBA, 8);

        // Create a OpenGL texture identifier
        GLuint image_texture;
        glGenTextures(1, &image_texture);
        glBindTexture(GL_TEXTURE_2D, image_texture);

        // Setup filtering parameters for display
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

        // Upload pixels into texture
        glPixelStorei(GL_UNPACK_ROW_LENGTH, 0);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, image.data());
        
        imageID = (void*)image_texture;
    }

    virtual void ContextMenu()
    {
        if (ImGui::MenuItem("Load Image")) {
            filebrowser.Open();
        }
        if (ImGui::BeginMenu("Change Filter"))
        {
            if (ImGui::MenuItem("Nearest")) {
                glBindTexture(GL_TEXTURE_2D, (GLint)imageID);
                glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
                glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
            }
            if (ImGui::MenuItem("Linear")) {
                glBindTexture(GL_TEXTURE_2D, (GLint)imageID);
                glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
                glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
            }
            ImGui::EndMenu();
        }
    }

    // Inherited via Node
    virtual void Render(NodeEditor* editor, NodeKey id, ImVec2 offset) override;
};