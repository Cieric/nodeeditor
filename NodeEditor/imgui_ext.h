#pragma once
#include <imgui.h>
#include <algorithm>

static inline ImVec2 operator+(const ImVec2& lhs, const ImVec2& rhs)
{
    return ImVec2(lhs.x + rhs.x, lhs.y + rhs.y);
}

static inline ImVec2 operator-(const ImVec2& lhs, const ImVec2& rhs)
{
    return ImVec2(lhs.x - rhs.x, lhs.y - rhs.y);
}

static inline ImVec2 operator-(const ImVec2& lhs, const float& rhs)
{
    return ImVec2(lhs.x - rhs, lhs.y - rhs);
}

static inline ImVec2 operator*(const ImVec2& lhs, const float& rhs)
{
    return ImVec2(lhs.x * rhs, lhs.y * rhs);
}

static inline ImVec2 operator*(const ImVec2& lhs, const ImVec2& rhs)
{
    return ImVec2(lhs.x * rhs.x, lhs.y * rhs.y);
}

static inline ImVec2 operator/(const ImVec2& lhs, const ImVec2& rhs)
{
    return ImVec2(lhs.x / rhs.x, lhs.y / rhs.y);
}

static inline ImVec2 operator/(const ImVec2& lhs, const float& rhs)
{
    return ImVec2(lhs.x / rhs, lhs.y / rhs);
}

static inline ImVec2 operator/=(ImVec2& lhs, const float& rhs)
{
    return lhs = ImVec2(lhs.x / rhs, lhs.y / rhs);
}

static inline ImVec2 operator*=(ImVec2& lhs, const float& rhs)
{
    return lhs = ImVec2(lhs.x * rhs, lhs.y * rhs);
}

static inline ImVec2 operator+=(ImVec2& lhs, const ImVec2& rhs)
{
    return lhs = ImVec2(lhs.x + rhs.x, lhs.y + rhs.y);
}

static inline ImVec2 normalize(ImVec2& lhs)
{
    return lhs /= sqrtf(lhs.x * lhs.x + lhs.y * lhs.y);
}

static inline float length(const ImVec2& lhs)
{
    return sqrtf(lhs.x * lhs.x + lhs.y * lhs.y);
}

static inline float distance(const ImVec2& lhs, const ImVec2& rhs)
{
    return length(rhs - lhs);
}

static inline ImVec2 normalized(const ImVec2& lhs)
{
    return lhs / sqrtf(lhs.x * lhs.x + lhs.y * lhs.y);
}

static inline ImVec2 getBezierPoint(const ImVec2& p1, const ImVec2& p2, const ImVec2& p3, const ImVec2& p4, const float& t)
{
    float ti = (1.0f - t);
    ImVec2 ip1 = p4 * (ti * ti * ti);
    ImVec2 ip2 = p3 * (ti * ti * t) * 3.0f;
    ImVec2 ip3 = p2 * (ti * t * t) * 3.0f;
    ImVec2 ip4 = p1 * (t * t * t);

    return ip1 + ip2 + ip3 + ip4;
}

static inline float getBezierDistance(const ImVec2& p1, const ImVec2& p2, const ImVec2& p3, const ImVec2& p4, const ImVec2& point)
{
    float len = INFINITY;
    for (float t = 0.0; t < 1.0; t += 1.0f/50.0f)
    {
        ImVec2 p = getBezierPoint(p1, p2, p3, p4, t);
        len = std::min(length(point - p), len);
    }

    return len;
}