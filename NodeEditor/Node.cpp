#include "Node.h"
#include "NodeEditor.h"

void Node::Render(NodeEditor* edito, NodeKey id, ImVec2 offset)
{
    NodeEditor* editor = (NodeEditor*)edito;
    ImGui::PushID(id);

    ImGuiIO& io = ImGui::GetIO();

    ImDrawList* draw_list = ImGui::GetWindowDrawList();

    draw_list->ChannelsSplit(2);
    draw_list->ChannelsSetCurrent(0);

    ImVec2 node_rect_min = offset + Pos;

    draw_list->ChannelsSetCurrent(1);
    bool old_any_active = ImGui::IsAnyItemActive();
    ImGui::SetCursorScreenPos(node_rect_min + NODE_WINDOW_PADDING);
    ImGui::BeginGroup();
    ImGui::Text("%s", Name);
    ImGui::EndGroup();

    bool node_widgets_active = (!old_any_active && ImGui::IsAnyItemActive());
    Size = ImGui::GetItemRectSize() + NODE_WINDOW_PADDING + NODE_WINDOW_PADDING;
    ImVec2 node_rect_max = node_rect_min + Size;

    draw_list->ChannelsSetCurrent(0);
    ImGui::SetCursorScreenPos(node_rect_min);
    ImGui::InvisibleButton("node", Size);
    if (ImGui::IsItemHovered())
    {
        editor->node_hovered_in_scene = id;
        editor->open_context_menu |= ImGui::IsMouseClicked(1);
    }

    bool node_moving_active = ImGui::IsItemActive();
    if (node_widgets_active || node_moving_active)
        editor->node_selected = id;
    if (node_moving_active && ImGui::IsMouseDragging(ImGuiMouseButton_Left) && editor->linkIdx == NullKey)
        Pos += io.MouseDelta;

    ImU32 node_bg_color = (editor->node_hovered_in_list == id || editor->node_hovered_in_scene == id || (editor->node_hovered_in_list == NullKey && editor->node_selected == id)) ? IM_COL32(75, 75, 75, 255) : IM_COL32(60, 60, 60, 255);

    draw_list->AddRectFilled(node_rect_min, node_rect_max, node_bg_color, 4.0f);
    draw_list->AddRect(node_rect_min, node_rect_max, IM_COL32(100, 100, 100, 255), 4.0f);

    for (size_t slot_idx = 0; slot_idx < InputsCount + OutputsCount; slot_idx++)
    {
        auto pos = offset + GetSlotPos(slot_idx);
        draw_list->AddCircleFilled(pos, NODE_SLOT_RADIUS, IM_COL32(150, 150, 150, 255));
    }

    ImGui::PopID();
    draw_list->ChannelsMerge();
}

void NodeLink::Render(NodeEditor* editor, ImVec2 offset)
{
    ImDrawList* draw_list = ImGui::GetWindowDrawList();

    Node* node_inp = editor->nodes[InputIdx];
    Node* node_out = editor->nodes[OutputIdx];
    ImVec2 p0 = offset + node_inp->GetSlotPos(InputSlot);
    ImVec2 p1 = p0+node_inp->GetSlotNormal(InputSlot);
    ImVec2 p3 = offset + node_out->GetSlotPos(OutputSlot);
    ImVec2 p2 = p3+node_out->GetSlotNormal(OutputSlot);

    if (isDelayed)
    {
        draw_list->AddBezierCurve(p0, p1, p2, p3, IM_COL32(0, 0, 0, 255), 8.0f);
        draw_list->AddBezierCurve(p0, p1, p2, p3, IM_COL32(200, 200, 100, 255), 4.0f);
    }
    else
    {
        auto s0 = getBezierPoint(p0, p1, p2, p3, 0.5);
        auto const l = 16;
        auto s0n = normalized(s0 - getBezierPoint(p0, p1, p2, p3, 0.45f));
        auto s0t = ImVec2(s0n.y, -s0n.x) / 2.0f;

        s0n *= 2.0f;
        s0t *= 18.0f;

        draw_list->AddBezierCurve(p0, p1, p2, p3, IM_COL32(0, 0, 0, 255), 8.0f);
        draw_list->AddLine(s0 - s0n - s0t, s0 - s0n + s0t, IM_COL32(0, 0, 0, 255), 4.0f);
        draw_list->AddLine(s0 + s0n - s0t, s0 + s0n + s0t, IM_COL32(0, 0, 0, 255), 4.0f);

        s0t *= 16.0F/18.0f;

        draw_list->AddBezierCurve(p0, p1, p2, p3, IM_COL32(200, 100, 100, 255), 4.0f);
        draw_list->AddLine(s0 - s0n - s0t, s0 - s0n + s0t, IM_COL32(200, 100, 100, 255), 2.0f);
        draw_list->AddLine(s0 + s0n - s0t, s0 + s0n + s0t, IM_COL32(200, 100, 100, 255), 2.0f);

        //draw_list->AddCircleFilled(s0 + s0n, 6, IM_COL32(200, 200, 100, 255));
        //draw_list->AddCircleFilled(s0 - s0n, 6, IM_COL32(200, 200, 100, 255));
    }
}
