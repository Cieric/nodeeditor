#pragma once

#include "Node.h"

struct InputNode : public Node
{
    InputNode() { throw "never call default constructor"; };

    InputNode(const char* name, const ImVec2& pos) : Node(name, pos, 0, 1)
    {}

    virtual void Render(NodeEditor* editor, NodeKey id, ImVec2 offset) override;

};