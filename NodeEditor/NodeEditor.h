#pragma once

#include <experimental/unordered_map>

#include <imgui.h>
#include "imgui_ext.h"

#include "Node.h"
#include "AdderNode.h"
#include "TextureNode.h"
#include "InputNode.h"
#include "DelayNode.h"
#include "HandleMap.h"

#include "AStar.h"

const float NODE_SLOT_RADIUS = 5.0f;
const ImVec2 NODE_WINDOW_PADDING = ImVec2(8.0f, 8.0f);

class NodeEditor
{

public:
    ImVec2 scrolling = ImVec2(0.0f, 0.0f);
    bool open_context_menu = false;
    NodeKey node_hovered_in_list = NullKey;
    NodeKey node_hovered_in_scene = NullKey;
    NodeKey node_selected = NullKey;
    NodeKey linkIdx = NullKey;
    NodeKey link_selected = NullKey;

    std::string windowName = "";

    bool opened = true;
    HandleMap<Node*, NodeKey> nodes;
    HandleMap<NodeLink, NodeKey> links;
    bool show_grid = true;

public:
    NodeEditor(std::string windowName) : windowName(windowName)
    {
        //auto n0 = nodes.insert(new TextureNode("MainTex", ImVec2(40, 50)));
        //auto n1 = nodes.insert(new TextureNode("BumpMap", ImVec2(40, 250)));
        //auto n2 = nodes.insert(new Node("Combine", ImVec2(270, 80), 2, 2));
        //links.insert(NodeLink(n0, 1, n2, 0));
        //links.insert(NodeLink(n1, 1, n2, 1));
    }

    int inline GetIntersection(float fDst1, float fDst2, ImVec2 P1, ImVec2 P2, ImVec2& Hit) {
        if ((fDst1 * fDst2) >= 0.0f) return 0;
        if (fDst1 == fDst2) return 0;
        Hit = P1 + (P2 - P1) * (-fDst1 / (fDst2 - fDst1));
        return 1;
    }

    int inline InBox(ImVec2 Hit, ImVec2 B1, ImVec2 B2, const int Axis) {
        if (Axis == 1 && Hit.y > B1.y && Hit.y < B2.y) return 1;
        if (Axis == 2 && Hit.x > B1.x && Hit.x < B2.x) return 1;
        return 0;
    }

    // returns true if line (L1, L2) intersects with the box (B1, B2)
    // returns intersection point in Hit
    bool CheckLineBox(ImVec2 B1, ImVec2 B2, ImVec2 L1, ImVec2 L2)
    {
        if (L2.x < B1.x && L1.x < B1.x) return false;
        if (L2.x > B2.x && L1.x > B2.x) return false;
        if (L2.y < B1.y && L1.y < B1.y) return false;
        if (L2.y > B2.y && L1.y > B2.y) return false;
        if (L1.x > B1.x && L1.x < B2.x &&
            L1.y > B1.y && L1.y < B2.y)
        {
            return true;
        }

        ImVec2 Hit;
        if ((GetIntersection(L1.x - B1.x, L2.x - B1.x, L1, L2, Hit) && InBox(Hit, B1, B2, 1))
            || (GetIntersection(L1.y - B1.y, L2.y - B1.y, L1, L2, Hit) && InBox(Hit, B1, B2, 2))
            || (GetIntersection(L1.x - B2.x, L2.x - B2.x, L1, L2, Hit) && InBox(Hit, B1, B2, 1))
            || (GetIntersection(L1.y - B2.y, L2.y - B2.y, L1, L2, Hit) && InBox(Hit, B1, B2, 2)))
            return true;

        return false;
    }

    void DrawCatmull(ImVec2 p0, ImVec2 p1, ImVec2 p2, ImVec2 p3, ImU32 color, float radius, int segments = 12)
    {
        ImDrawList* draw_list = ImGui::GetWindowDrawList();
        float alpha = 0.5f;
        float tension = 0.0f;

        float t01 = pow(distance(p0, p1), alpha);
        float t12 = pow(distance(p1, p2), alpha);
        float t23 = pow(distance(p2, p3), alpha);

        ImVec2 m1 = (p2 - p1 + ((p1 - p0) / t01 - (p2 - p0) / (t01 + t12)) * t12) * (1.0f - tension);
        ImVec2 m2 = (p2 - p1 + ((p3 - p2) / t23 - (p3 - p1) / (t12 + t23)) * t12) * (1.0f - tension);

        ImVec2 a = (p1 - p2) * 2.0f + m1 + m2;
        ImVec2 b = (p1 - p2) * -3.0f - m1 - m1 - m2;
        ImVec2 c = m1;
        ImVec2 d = p1;

        float delta = 1.0f / (float)segments;

        for (float t = 0.0; t < 1.0; t += delta)
        {
            ImVec2 p1 = ((a * t + b) * t + c) * t + d;
            ImVec2 p2 = ((a * (t + delta) + b) * (t + delta) + c) * (t + delta) + d;
            draw_list->AddLine(p1, p2, color, radius);
        }
    }

    ImVec2 GetCatmullPos(const std::vector<ImVec2>& path, float t)
    {
        float alpha = 0.5f;
        float tension = 0.0f;

        float bigt = t * (float)(path.size() - 3) + 1.0f;

        auto& p0 = path[(int)floor(bigt)-1];
        auto& p1 = path[(int)floor(bigt)];
        auto& p2 = path[(int)ceil(bigt)];
        auto& p3 = path[(int)ceil(bigt)+1];

        float t01 = pow(distance(p0, p1), alpha);
        float t12 = pow(distance(p1, p2), alpha);
        float t23 = pow(distance(p2, p3), alpha);

        ImVec2 m1 = (p2 - p1 + ((p1 - p0) / t01 - (p2 - p0) / (t01 + t12)) * t12) * (1.0f - tension);
        ImVec2 m2 = (p2 - p1 + ((p3 - p2) / t23 - (p3 - p1) / (t12 + t23)) * t12) * (1.0f - tension);

        ImVec2 a = (p1 - p2) * 2.0f + m1 + m2;
        ImVec2 b = (p1 - p2) * -3.0f - m1 - m1 - m2;

        t = fmod(bigt, 1.0f);

        return ((a * t + b) * t + m1) * t + p1;
    }

    void DrawCatmull(const std::vector<ImVec2>& path, ImU32 color, float radius, size_t segments = 12)
    {
        ImDrawList* draw_list = ImGui::GetWindowDrawList();
        for (size_t i = 0; i < segments; i++)
        {
            ImVec2 start = GetCatmullPos(path, (float)i / segments);
            ImVec2 end = GetCatmullPos(path, (float)(i + 1) / segments);
            draw_list->AddLine(start, end, color, radius);
        }

        for (size_t i = 0; i < segments+1; i++)
        {
            ImVec2 pos = GetCatmullPos(path, (float)i / segments);
            draw_list->AddCircleFilled(pos, radius, color);
        }
    }

    std::vector<ImVec2> FindRoute(ImVec2 start, ImVec2 end)
    {
        AStar as;
        std::vector<std::pair<ImVec2, ImVec2>> AsObstacle;

        for (auto node : nodes) {
            auto tl = node.second->Pos + scrolling + ImVec2(8, 28);
            auto br = tl + node.second->Size;
            AsObstacle.push_back(std::make_pair(tl, br));
            tl += ImVec2(-1, -1);
            br += ImVec2(1, 1);
            as.addPos(tl);
            as.addPos(br);
            as.addPos(ImVec2(br.x, tl.y));
            as.addPos(ImVec2(tl.x, br.y));
        }

        as.addPos(start);
        as.addPos(end);

        as.nodeStart = &as.nodes[as.nodes.size() - 2];
        as.nodeEnd = &as.nodes[as.nodes.size() - 1];

        ImDrawList* draw_list = ImGui::GetWindowDrawList();
        
        const int size = as.nodes.size();
        for (int i=0; i < size - 1; i++)
        {
            auto& node1 = as.nodes[i];
            for (int j = i + 1; j < size; j++)
            {
                auto& node2 = as.nodes[j];
                bool collide = false;
                for (auto Ob : AsObstacle)
                {
                    collide = collide || CheckLineBox(Ob.first, Ob.second, node1.pos, node2.pos);
                    auto pos = (node1.pos + node2.pos) / 2.0f;
                    collide = collide || InBox(pos, Ob.first, Ob.second, 1) && InBox(pos, Ob.first, Ob.second, 2);
                }
                if (!collide)
                {
                    as.nodes[i].vecNeighbours.push_back(&as.nodes[j]);
                    as.nodes[j].vecNeighbours.push_back(&as.nodes[i]);
                    //draw_list->AddLine(node1.pos, node2.pos, IM_COL32(0, 100, 200, 255), 2.0f);
                }
            }
        }

        std::vector<ImVec2> path;

        if (as.Solve())
        {
            auto currentNode = as.nodeEnd;
            
            while (currentNode != nullptr)
            {
                path.push_back(currentNode->pos);
                //draw_list->AddCircleFilled(currentNode->pos, 8, IM_COL32(200, 100, 0, 255));
                currentNode = currentNode->parent;
            }
        }

        return path;
    }

    void Update()
    {
        ImGui::SetNextWindowSize(ImVec2(700, 600), ImGuiCond_FirstUseEver);
        if (!ImGui::Begin(windowName.c_str(), &opened))
        {
            ImGui::End();
            return;
        }

        ImGuiIO& io = ImGui::GetIO();

        open_context_menu = false;

        const ImVec2 offset = ImGui::GetCursorScreenPos() + scrolling;
        
        ImGui::BeginGroup();

        ImGui::Text("Hold middle mouse button to scroll (%.2f,%.2f)", scrolling.x, scrolling.y);
        ImGui::SameLine(ImGui::GetWindowWidth() - 100);
        ImGui::Checkbox("Show grid", &show_grid);
        ImGui::PushStyleVar(ImGuiStyleVar_FramePadding, ImVec2(1, 1));
        ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding, ImVec2(0, 0));
        ImGui::PushStyleColor(ImGuiCol_ChildBg, IM_COL32(60, 60, 70, 200));
        ImGui::BeginChild("scrolling_region", ImVec2(0, 0), true, ImGuiWindowFlags_NoScrollbar | ImGuiWindowFlags_NoMove);
        ImGui::PopStyleVar();
        ImGui::PushItemWidth(120.0f);

        ImDrawList* draw_list = ImGui::GetWindowDrawList();

        if (show_grid)
        {
            ImU32 GRID_COLOR = IM_COL32(200, 200, 200, 40);
            float GRID_SZ = 64.0f;
            ImVec2 win_pos = ImGui::GetCursorScreenPos();
            ImVec2 canvas_sz = ImGui::GetWindowSize();
            for (float x = fmodf(scrolling.x, GRID_SZ); x < canvas_sz.x; x += GRID_SZ)
                draw_list->AddLine(ImVec2(x, 0.0f) + win_pos, ImVec2(x, canvas_sz.y) + win_pos, GRID_COLOR);
            for (float y = fmodf(scrolling.y, GRID_SZ); y < canvas_sz.y; y += GRID_SZ)
                draw_list->AddLine(ImVec2(0.0f, y) + win_pos, ImVec2(canvas_sz.x, y) + win_pos, GRID_COLOR);
        }

        ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding, ImVec2(8, 8));
        if (ImGui::BeginPopup("context_menu"))
        {
            ImVec2 scene_pos = ImGui::GetMousePosOnOpeningCurrentPopup() - offset;
            if (Node* node = node_selected != NullKey ? nodes[node_selected] : NULL)
            {
                ImGui::Text("Node '%s'", node->Name);
                ImGui::Separator();
                if (ImGui::MenuItem("Rename..", NULL, false)) {}
                if (ImGui::MenuItem("Delete", NULL, false)) {
                    std::experimental::erase_if(links, [&](auto link) {
                        bool erase = link.second.InputIdx == node_selected || link.second.OutputIdx == node_selected;
                        if (erase)
                        {
                            auto& inputNode = *nodes[link.second.InputIdx];
                            auto& outputNode = *nodes[link.second.OutputIdx];
                            inputNode.DisconnectNode(link.second.InputSlot, outputNode, link.second.OutputSlot);
                            outputNode.DisconnectNode(link.second.OutputSlot, inputNode, link.second.InputSlot);
                        }
                        return erase;
                    });
                    nodes.erase(node_selected);
                }
                ImGui::Separator();
                node->ContextMenu();
            }
            else if (NodeLink* link = link_selected != NullKey ? &links[link_selected] : NULL)
            {
                if (ImGui::MenuItem("Delete", NULL, false)) {
                    auto& inputNode = *nodes[ link->InputIdx];
                    auto& outputNode = *nodes[link->OutputIdx];
                    inputNode.DisconnectNode(link->InputSlot, outputNode, link->OutputSlot);
                    outputNode.DisconnectNode(link->OutputSlot, inputNode, link->InputSlot);
                    links.erase(link_selected);
                }
            }
            else if (ImGui::BeginMenu("Add Node"))
            {
                if (ImGui::MenuItem("Input Node", NULL, false)) {
                    nodes.insert(new InputNode("Input Node", scene_pos));
                }
                if (ImGui::MenuItem("Adder Node", NULL, false)) {
                    nodes.insert(new AdderNode("Adder", scene_pos));
                }
                if (ImGui::MenuItem("Texture Node", NULL, false)) {
                    nodes.insert(new TextureNode("Texture Node", scene_pos));
                }
                if (ImGui::MenuItem("Delay Node", NULL, false)) {
                    nodes.insert(new DelayNode("Delay Node", scene_pos));
                }
                ImGui::EndMenu();
            }
            ImGui::EndPopup();
        }
        ImGui::PopStyleVar();

        for (auto idlink : links)
        {
            NodeLink& link = idlink.second;
            Node* node_inp = nodes[link.InputIdx];
            Node* node_out = nodes[link.OutputIdx];
            ImVec2 p1 = offset + node_inp->GetSlotPos(link.InputSlot);
            ImVec2 p2 = offset + node_out->GetSlotPos(link.OutputSlot);
            ImVec2 p1n = node_inp->GetSlotNormal(link.InputSlot);
            ImVec2 p2n = node_out->GetSlotNormal(link.OutputSlot);

            if (ImGui::IsMouseClicked(ImGuiMouseButton_Right) && getBezierDistance(p1, p1 + p1n, p2 + p2n, p2, ImGui::GetMousePos()) < 6.0)
            {
                node_hovered_in_list = NullKey;
                node_hovered_in_scene = NullKey;
                node_selected = NullKey;

                linkIdx = NullKey;
                link_selected = idlink.first;
                open_context_menu = true;
            }

            auto path = FindRoute(p1 + p1n * 2.0f, p2 + p2n * 2.0f);
            if (path.size() < 2)
                continue;

            std::vector<ImVec2> path2;
            path2.push_back(p1 - p1n * 16.0f);
            path2.push_back(path[0]);
            path2.push_back(p2 + p2n * 16.0f);
            path2.insert(path2.end(), path.begin() + 1, path.end() - 1);
            //for (size_t i = 1; i < path.size() - 1; i++)
            //    path2.push_back(path[i]);
            path2.push_back(p1 + p1n * 16.0f);
            path2.push_back(path[path.size() - 1]);
            path2.push_back(p2 - p2n * 16.0f);

            DrawCatmull(path2, IM_COL32(0, 0, 0, 255), 6.0f, 24);
            DrawCatmull(path2, IM_COL32(200, 100, 0, 255), 3.0f, 24);
        }

        if(linkIdx != NullKey && node_selected != NullKey)
        {
            Node* node_out = nodes[node_selected];
            ImVec2 normal = node_out->GetSlotNormal(linkIdx);
            ImVec2 p1 = ImGui::GetMousePos();
            //ImVec2 p2 = p1 - normal;
            ImVec2 p4 = offset + node_out->GetSlotPos(linkIdx);
            //ImVec2 p3 = p4 + normal;

            auto path = FindRoute(p1 - normal, p4 + normalized(normal));
            path.push_back(p4 + normal);

            ImVec2 p0 = p1 - normal;
            for (int i = -1; i < (int)path.size() - 3; i++)
            {
                auto p1 = path[i+1];
                auto p2 = path[i+2];
                auto p3 = path[i+3];
                DrawCatmull(p0, p1, p2, p3, IM_COL32(200, 200, 100, 255), 3.0f);
                p0 = p1;
            }

            //draw_list->AddBezierCurve(p1, p2, p3, p4, IM_COL32(200, 200, 100, 255), 3.0f);
        }

        draw_list->ChannelsMerge();

        for (auto& idnode : nodes)
        {
            for (size_t slot_idx = 0; slot_idx < idnode.second->InputsCount + idnode.second->OutputsCount; slot_idx++)
            {
                auto pos = offset + idnode.second->GetSlotPos(slot_idx);
                //draw_list->AddCircleFilled(pos, NODE_SLOT_RADIUS, IM_COL32(150, 150, 150, 255));
                ImGui::SetCursorScreenPos(pos - NODE_SLOT_RADIUS);
                ImGui::InvisibleButton("node", ImVec2(2 * NODE_SLOT_RADIUS, 2 * NODE_SLOT_RADIUS));
                if (ImGui::IsItemHovered() &&
                    ImGui::IsMouseClicked(ImGuiMouseButton_Left) &&
                    idnode.second->CanConnectNode(slot_idx))
                {
                    linkIdx = slot_idx;
                    node_selected = idnode.first;
                }

                if (ImGui::IsItemHovered() &&
                    ImGui::IsMouseReleased(ImGuiMouseButton_Left) &&
                    idnode.second->CanConnectNode(slot_idx) &&
                    node_selected != NullKey &&
                    node_selected != idnode.first &&
                    linkIdx != NullKey)
                {

                    if (nodes[node_selected]->ConnectNode(linkIdx, *idnode.second, slot_idx))
                    {
                        if (idnode.second->ConnectNode(slot_idx, *nodes[node_selected], linkIdx))
                        {
                            links.insert(NodeLink(node_selected, linkIdx, idnode.first, slot_idx));
                        }
                        else
                        {
                            nodes[node_selected]->DisconnectNode(linkIdx, *idnode.second, slot_idx);
                        }
                    }
                }
            }

            idnode.second->Render(this, idnode.first, offset);
        }

        if(ImGui::IsMouseReleased(ImGuiMouseButton_Left))
        {
            linkIdx = NullKey;
            node_selected = NullKey;
        }

        if (ImGui::IsMouseClicked(ImGuiMouseButton_Right) && !open_context_menu)
        {
            if (ImGui::IsWindowHovered(ImGuiHoveredFlags_AllowWhenBlockedByPopup) || !ImGui::IsAnyItemHovered())
            {
                node_selected = node_hovered_in_list = node_hovered_in_scene = NullKey;
                linkIdx = NullKey;
                link_selected = NullKey;
                open_context_menu = true;
            }
        }

        if (open_context_menu)
        {
            ImGui::OpenPopup("context_menu");
            if (node_hovered_in_list != NullKey)
                node_selected = node_hovered_in_list;
            if (node_hovered_in_scene != NullKey)
                node_selected = node_hovered_in_scene;
        }

        if (ImGui::IsWindowHovered() && !ImGui::IsAnyItemActive() && ImGui::IsMouseDragging(ImGuiMouseButton_Middle, 0.0f))
            scrolling = scrolling + io.MouseDelta;

        ImGui::PopItemWidth();
        ImGui::EndChild();
        ImGui::PopStyleColor();
        ImGui::PopStyleVar();
        ImGui::EndGroup();

        ImGui::End();
    }
};