#pragma once

#include <algorithm>

constexpr GuiID NullGuiID = std::numeric_limits<GuiID>().max();
typedef int GuiID;

class GuiItem
{
	GuiID id;
	virtual void Render() = 0;
	virtual bool OnClick(int i) = 0;
};