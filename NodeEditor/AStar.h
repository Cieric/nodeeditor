#pragma once
#include <vector>
#include <list>
#include <imgui.h>
#include "imgui_ext.h"

struct AStar
{
	struct sNode
	{
		bool bObstacle = false;
		bool bVisited = false;
		float fGlobalGoal = INFINITY;
		float fLocalGoal = INFINITY;
		ImVec2 pos;
		std::vector<sNode*> vecNeighbours;
		sNode* parent = nullptr;
	};

	std::vector<sNode> nodes;
	sNode* nodeStart = nullptr;
	sNode* nodeEnd = nullptr;

	void addPos(ImVec2 pos)
	{
		sNode node;
		node.pos = pos;
		nodes.push_back(node);
	}

	bool Solve()
	{
		for (auto node : nodes)
		{
			node.bVisited = false;
			node.fGlobalGoal = INFINITY;
			node.fLocalGoal = INFINITY;
			node.parent = nullptr;
		}

		auto distance = [](sNode* a, sNode* b)
		{
			auto diff = a->pos - b->pos;
			return sqrtf(diff.x * diff.x + diff.y * diff.y);
		};

		auto heuristic = [distance](sNode* a, sNode* b)
		{
			return distance(a, b);
		};

		sNode* nodeCurrent = nodeStart;
		nodeStart->fLocalGoal = 0.0f;
		nodeStart->fGlobalGoal = heuristic(nodeStart, nodeEnd);

		std::list<sNode*> listNotTestedNodes;
		listNotTestedNodes.push_back(nodeStart);

		while (!listNotTestedNodes.empty() && nodeCurrent != nodeEnd)
		{
			listNotTestedNodes.sort([](const sNode* lhs, const sNode* rhs) { return lhs->fGlobalGoal < rhs->fGlobalGoal; });

			while (!listNotTestedNodes.empty() && listNotTestedNodes.front()->bVisited)
				listNotTestedNodes.pop_front();

			if (listNotTestedNodes.empty())
				break;

			nodeCurrent = listNotTestedNodes.front();
			nodeCurrent->bVisited = true;
			
			for (auto nodeNeighbour : nodeCurrent->vecNeighbours)
			{
				if (!nodeNeighbour->bVisited && nodeNeighbour->bObstacle == 0)
					listNotTestedNodes.push_back(nodeNeighbour);

				float fPossiblyLowerGoal = nodeCurrent->fLocalGoal + distance(nodeCurrent, nodeNeighbour);

				if (fPossiblyLowerGoal < nodeNeighbour->fLocalGoal)
				{
					nodeNeighbour->parent = nodeCurrent;
					nodeNeighbour->fLocalGoal = fPossiblyLowerGoal;
					nodeNeighbour->fGlobalGoal = nodeNeighbour->fLocalGoal + heuristic(nodeNeighbour, nodeEnd);
				}
			}
		}

		return true;
	}
};
