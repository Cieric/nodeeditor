#pragma once

#include <unordered_map>

template<typename T, typename K = uint64_t>
class HandleMap : public std::unordered_map<K, T>
{
	K index = 0;
public:
	HandleMap() {};

	K insert(T elem)
	{
		K temp = index++;
		this->insert_or_assign(temp, elem);
		return temp;
	}

	T& operator[](K key)
	{
		if (this->find(key) == this->end())
			throw "Error no element at key";
		return (std::unordered_map<K, T>::operator[])(key);
	}
};
